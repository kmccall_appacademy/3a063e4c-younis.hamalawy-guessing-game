# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.

def guessing_game
  guess = rand(1..100)
  counter = 0
  user_guess = 0
  until user_guess == guess
    puts "Please guess a number between 1 & 100"
    user_guess = gets.chomp.to_i
    counter += 1

    if user_guess < guess
      puts user_guess
      puts "too low"
    elsif user_guess > guess
      puts user_guess
      puts "too high"
    end
  end
  puts "You won"
  puts guess
  puts counter
end

def file_shuffler
  file_name = gets.chomp
  contents = File.readlines(file_name)
  File.open("#{file_name}-shuffled.txt", "w") do |f|
    f.puts contents.shuffle
  end
end

file_shuffler
